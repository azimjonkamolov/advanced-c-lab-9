// Name: Lab_9.1.c
// Time: 16:02 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to sum int from file and find average and how many numbers are there

#include <stdio.h>
int main(void) {
    int i=0,size,a, so=0, b;
    FILE *fp1, *fp2;
    char temp;
    float avg=0.0;
	fp1=fopen("Data_1.txt", "w+");		// TO OPEN FILE TO STORE INTS IN
    do
	{
        scanf("%d%c", &a, &temp);		// TO ENTER INT AND CHECK IF THERE IS AN INPUT AS '\N' WITH TEMP
        fprintf(fp1, "%d ", a);
        i++; 
    } while(temp!= '\n'); // IF TEMP == N, LEAVE THE LOOP
	fclose(fp1); // TO CLOSE THE FILE
	fp2=fopen("Data_1.txt", "r");
	while(fscanf(fp2, "%d", &b) ==1) // TO SUM INTS IN THE FILE
    {
        so += b;
    }
    printf("%d\n", i); // TO SHOW HOW MANY INPUTS IN
    printf("%d\n", so); // TO SHOW THE SUM
    avg=so/i;			// TO GET THE AVERAGE WITH FLOAT VALUE
    printf("%.2f\n", avg); // TO SHOW THE AVERAGE
    
	return 0;
}
