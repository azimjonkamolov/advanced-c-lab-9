// Name: Lab_9.5.c
// Time: 17:54 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to delete a give live from a file 

#include <stdio.h>
#include <string.h>

#define ASIZE 500

  int main() 
  {
    FILE *fp1, *fp2;        
	int lnum, ctr=0;
	char str[ASIZE], hold[]="Hold.txt", fname[]="Data_5.txt", ch; // 2 FILES TO HOLD AND READ
	// (PLEASE GIVE FILE NAMES ^)
    fp1 = fopen(fname, "r"); // TO READ, FILE NAME fname
    fp2 = fopen(hold, "w"); // TO HOLD, FILE NAME hold
    scanf("%d", &lnum);	 // TO GIVE A DELETE LINE
    while (!feof(fp1)) 
    {
        strcpy(str, "\0"); // TO COPY EVERYTHING FROM THE FIRST FILE
        fgets(str, ASIZE, fp1);
   	    if (!feof(fp1)) 
        {
            ctr++;
            if (ctr != lnum) 
            {
                fprintf(fp2, "%s", str); // TO STORE IT INTO THE SECOND FILE
            }
        }
    }
    fclose(fp1);
    fclose(fp2);
    remove("Data_5.txt");  		// TO REMOVE THE ORIGINAL FILE 
    rename(hold, "Data_5.txt"); 	// TO RENAME THE HOLD FILE TO ORIGINAL
    // CHANGES HAPPENED :)
    fclose(fp1);
    return 0;

  } 
