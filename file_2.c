// Name: Lab_9.2.c
// Time: 16:22 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to change letters in a file if up, lower if lower, up

#include<stdio.h>
#include<string.h>

int main()
{
	int a,b;
	char c,d;
	FILE *fp1, *fp2;
	fp1=fopen("Data_2_1.txt", "r");	// TO READ A FILE (PLEASE MAKE FILE WITH THE SAME NAME AND ANY INPUT IN IT)
	fp2=fopen("Data_2_2.txt", "w"); // TO OPEN A FILE TO WRITE VICE VERCA
	while((c=getc(fp1))!=EOF) // TO READ UNTILL THE END
	{
		// THE CHANGES HAPPENS IN HERE 
		printf("%c", c);
		if(c>=65 && c<=90) // IF UPPER, THEN LOWER	 
		{
			c=c+32;
		}
		else if (c>=97 && c<=122) // IF LOWER, THEN UPPER
		{
			c=c-32;
		}
		fputc(c,fp2); // TO PUT INSIDE ANOTHER FILE
	}
	
	fclose(fp1);	
	fclose(fp2);
	
	return 0;
}
