// Name: Lab_9.6.c
// Time: 18:22 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to copy strings form two file and make them one in one file

#include <stdio.h>
#include <string.h>

#define A 50 // TO GIVE STORE SIZE FOR ARRAYS

  int main() 
  {
    FILE *fp1, *fp2, *fp3;        // FOR FILES
	char st1[A], st2[A];		// TO GET, STRINGS INSIDE FILES 
	
    fp1 = fopen("Data_6.1.txt", "r"); // TO READ THE FIRST FILE
    while (!feof(fp1))  // TO GET THE FIRST STRING
    {
        strcpy(st1, "\0");
        fgets(st1, A, fp1);
    }
    
    fp2 = fopen("Data_6.2.txt", "r"); // TO READ THE SECOND FILE
    while (!feof(fp2))  // TO GET THE SECOND STRING
    {
        strcpy(st2, "\0");
        fgets(st2, A, fp2);
    }
 	
	strcat(st1,st2); // TO MAKE STRINGS ONE INTO ST1 WITH STRCAT FUNC
	fp3 = fopen("Data_6.3.txt", "w"); // TO OPEN THE THIRD FILE TO STORE UNITED STRING 
	fputs(st1,fp3); // TO PUT STRING INTO THE FUNCTION 
	printf("\t\t<<<Done>>>\n"); // TO SHOW THE JOB IS DONE
	
    fclose(fp1);
    fclose(fp2);
    fclose(fp3);
    
    return 0;

  } 
