// Name: Lab_9.3.c
// Time: 17:30 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to get all form file and show how many lines there are

#include<stdio.h>
#include<string.h>

int main()
{
	int lines=0, ch=0;
	char c;
	FILE *fp1;
	fp1=fopen("Data_3.txt", "r"); // TO READ A FILE (PLEASE THE SAME NAME)
	while((c=getc(fp1))!=EOF) // TO READ AND PRINT OUT
	{
		printf("%c", c);
		if(c=='\n')
		{
			lines++; // TO COUNT LINES
		}
	}
	printf("\n");
	lines+=1; // FOR THE LAST LINE
	printf("Lines: %d\n", lines); // TO SHOW HOW MANY LINES THERE ARE
	
	fclose(fp1);
	return 0;
}
