// Name: Lab_9.4.c
// Time: 16:42 12.12.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to find out how many chars and lines there are

#include<stdio.h>
#include<string.h>

int main()
{
	int words=0, ch=0;
	char c;
	FILE *fp1;
	fp1=fopen("Data_4.txt", "r");
	while((c=getc(fp1))!=EOF)
	{
		printf("%c", c);
		if(c==32  || c=='\n')
		{
			words++;
		}
		ch++;
	}
	printf("\n");
	words+=1;
	printf("Words: %d\n", words);
	printf("Chars: %d\n", ch);
	
	fclose(fp1);
	return 0;
}
